﻿using System;
using System.Data;

namespace DBComparer.BLL
{
    public class CompareTableIndexes
    {
        string source_Compare_Tables_List = string.Empty;
        string destination_Compare_Tables_List = string.Empty;
        DataTable dt_Source_Tables = new DataTable();
        DataTable dt_Destination_Tables = new DataTable();
        DataTable dtExcelData = new DataTable();
        DAL.DAL dal = new DAL.DAL();
        public CompareTableIndexes(string source_Compare_Tables_List, string destination_Compare_Tables_List, DataTable dtExcelData)
        {
            this.source_Compare_Tables_List = source_Compare_Tables_List;
            this.destination_Compare_Tables_List = destination_Compare_Tables_List;
            this.dtExcelData = dtExcelData;
            dtExcelData.Columns.Add("SrNo");
            dtExcelData.Columns.Add("SourceTableName");
            dtExcelData.Columns.Add("SourceExist");
            dtExcelData.Columns.Add("SourceSrNo");
            dtExcelData.Columns.Add("SourceIndexType");
            dtExcelData.Columns.Add("SourceIndexName");
            dtExcelData.Columns.Add("SourceIndexedColumns");
            dtExcelData.Columns.Add("SourceIncludedColumns");
            dtExcelData.Columns.Add("DestinationTableName");
            dtExcelData.Columns.Add("DestinationExist");
            dtExcelData.Columns.Add("DestinationIndexExist");
            dtExcelData.Columns.Add("DestinationSrNo");
            dtExcelData.Columns.Add("DestinationIndexType");
            dtExcelData.Columns.Add("DestinationIndexName");
            dtExcelData.Columns.Add("DestinationIndexedColumns");
            dtExcelData.Columns.Add("DestinationIncludedColumns");
            dtExcelData.Columns.Add("Remarks");
        }

        public void Compare()
        {
            try
            {
                dt_Source_Tables = dal.GetTableList(source_Compare_Tables_List, DAL.DAL.ConnectionType.Source);
                dt_Destination_Tables = dal.GetTableList(destination_Compare_Tables_List, DAL.DAL.ConnectionType.Destination);

                string[] SourceTableList = source_Compare_Tables_List.Replace("'", "").Split(',');
                string[] DestinationTableList = destination_Compare_Tables_List.Replace("'", "").Split(',');

                for (int i = 0; i < SourceTableList.Length; i++)
                {
                    DataRow dr = null;
                    DataTable dt_Source_Index = new DataTable();
                    int SourceIndexCount = 0;
                    dr = dtExcelData.NewRow();
                    dr["SrNo"] = i + 1;
                    dr["SourceTableName"] = SourceTableList[i];
                    var sourceTableInfo = dt_Source_Tables.AsEnumerable().Where(x => x.Field<string>("TableName").Trim().ToLower() == SourceTableList[i].Trim().ToLower());
                    var destinationTableInfo = dt_Destination_Tables.AsEnumerable().Where(x => x.Field<string>("TableName").Trim().ToLower() == DestinationTableList[i].Trim().ToLower());

                    if (sourceTableInfo.AsDataView().Count > 0)
                    {
                        dr["SourceExist"] = "Yes";
                        dt_Source_Index = dal.GetTableIndexList(Convert.ToString(sourceTableInfo.CopyToDataTable().Rows[0]["TableId"]), DAL.DAL.ConnectionType.Source);
                        SourceIndexCount = dt_Source_Index.Rows.Count;

                        DataTable dt_Destination_Index = new DataTable();
                        int DestinationIndexCount = 0;
                        if (destinationTableInfo.AsDataView().Count > 0)
                        {
                            dr["DestinationTableName"] = DestinationTableList[i];
                            dr["DestinationExist"] = "Yes";
                            dt_Destination_Index = dal.GetTableIndexList(Convert.ToString(destinationTableInfo.CopyToDataTable().Rows[0]["TableId"]), DAL.DAL.ConnectionType.Destination);
                            DestinationIndexCount = dt_Destination_Index.Rows.Count;

                            for (int j = 0; j < dt_Source_Index.Rows.Count; j++)
                            {
                                string indexType = Convert.ToString(dt_Source_Index.Rows[j]["index_description"]);
                                string indexedColumns = Convert.ToString(dt_Source_Index.Rows[j]["indexed_columns"]);
                                string includedColumns = Convert.ToString(dt_Source_Index.Rows[j]["included_columns"]);

                                dr["SrNo"] = i + 1;
                                dr["SourceTableName"] = SourceTableList[i];
                                dr["SourceSrNo"] = j + 1;
                                dr["SourceIndexType"] = indexType;
                                dr["SourceIndexName"] = Convert.ToString(dt_Source_Index.Rows[j]["Index_Name"]);
                                dr["SourceIndexedColumns"] = indexedColumns;
                                dr["SourceIncludedColumns"] = includedColumns;

                                var destination_idx_check = dt_Destination_Index.AsEnumerable().Where(x => Convert.ToString(x.Field<string>("indexed_columns")).Trim().ToLower() == indexedColumns.Trim().ToLower() && Convert.ToString(x.Field<string>("index_description")).Trim().ToLower() == indexType.Trim().ToLower() && Convert.ToString(x.Field<string>("included_columns")).Trim().ToLower() == includedColumns.Trim().ToLower());
                                if (destination_idx_check.AsDataView().Count > 0)
                                {
                                    DataTable destination_idx_check_Table = destination_idx_check.CopyToDataTable();
                                    dr["DestinationSrNo"] = j + 1;
                                    dr["DestinationIndexType"] = Convert.ToString(destination_idx_check_Table.Rows[0]["index_description"]);
                                    dr["DestinationIndexName"] = Convert.ToString(destination_idx_check_Table.Rows[0]["index_name"]);
                                    dr["DestinationIndexedColumns"] = Convert.ToString(destination_idx_check_Table.Rows[0]["indexed_columns"]);
                                    dr["DestinationIncludedColumns"] = Convert.ToString(destination_idx_check_Table.Rows[0]["included_columns"]);
                                }
                                else
                                {
                                    dr["DestinationIndexExist"] = "No";
                                }
                                dtExcelData.Rows.Add(dr);

                                if (j + 1 < dt_Source_Index.Rows.Count)
                                {
                                    dr = dtExcelData.NewRow();
                                }
                            }
                            if (SourceIndexCount != DestinationIndexCount)
                            {
                                dr = dtExcelData.NewRow();
                                dr["SrNo"] = i + 1;
                                dr["SourceTableName"] = SourceTableList[i];
                                dr["Remarks"] = "Total Index count mismatch";
                                dtExcelData.Rows.Add(dr);
                            }
                            else if (SourceIndexCount == 0 && DestinationIndexCount == 0)
                            {
                                dtExcelData.Rows.Add(dr);
                            }
                        }
                        else
                        {
                            dr["DestinationTableName"] = DestinationTableList[i];
                            dr["DestinationExist"] = "No";

                            if (SourceIndexCount > 0)
                            {
                                for (int j = 0; j < dt_Source_Index.Rows.Count; j++)
                                {
                                    if (j < SourceIndexCount)
                                    {
                                        dr["SrNo"] = i + 1;
                                        dr["SourceTableName"] = SourceTableList[i];

                                        dr["SourceSrNo"] = j + 1;
                                        dr["SourceIndexType"] = Convert.ToString(dt_Source_Index.Rows[j]["index_description"]);
                                        dr["SourceIndexName"] = Convert.ToString(dt_Source_Index.Rows[j]["index_name"]);
                                        dr["SourceIndexedColumns"] = Convert.ToString(dt_Source_Index.Rows[j]["indexed_columns"]); ;
                                        dr["SourceIncludedColumns"] = Convert.ToString(dt_Source_Index.Rows[j]["included_columns"]); ;

                                    }
                                    dtExcelData.Rows.Add(dr);
                                    if (j + 1 < dt_Source_Index.Rows.Count)
                                    {
                                        dr = dtExcelData.NewRow();
                                    }
                                }
                            }
                            else
                            {
                                dtExcelData.Rows.Add(dr);
                            }
                        }
                    }
                    else
                    {
                        dr["SourceExist"] = "No";
                        dtExcelData.Rows.Add(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
