﻿using System;
using System.Data;

namespace DBComparer.BLL
{
    public class CompareSPs
    {
        string dt_Compare_SP_List = string.Empty;
        DataTable dt_Source_SP = new DataTable();
        DataTable dt_Destination_SP = new DataTable();
        DataTable dtExcelData = new DataTable();
        DAL.DAL dal = new DAL.DAL();
        public CompareSPs(string dt_Compare_SP_List, DataTable dtExcelData)
        {
            this.dt_Compare_SP_List = dt_Compare_SP_List;
            this.dtExcelData = dtExcelData;
            dtExcelData.Columns.Add("SrNo");
            dtExcelData.Columns.Add("SourceSPName");
            dtExcelData.Columns.Add("SourceExist");
            dtExcelData.Columns.Add("SourceSPCode");
            dtExcelData.Columns.Add("SourceSPCreatedDate");
            dtExcelData.Columns.Add("SourceSPLastAlteredDate");
            dtExcelData.Columns.Add("DestinationExist");
            dtExcelData.Columns.Add("DestinationSPName");
            dtExcelData.Columns.Add("DestinationSPCode");
            dtExcelData.Columns.Add("DestinationSPCreatedDate");
            dtExcelData.Columns.Add("DestinationSPLastAlteredDate");
            dtExcelData.Columns.Add("CodeMatch");

        }
        public void Compare()
        {
            try
            {
                int j = 0;
                dt_Source_SP = dal.GetSPList(dt_Compare_SP_List, DAL.DAL.ConnectionType.Source);
                dt_Destination_SP = dal.GetSPList(dt_Compare_SP_List, DAL.DAL.ConnectionType.Destination);

                string[] SPList = dt_Compare_SP_List.Replace("'", "").Split(',');

                for (int i = 0; i < SPList.Length; i++)
                {
                    DataRow dr = null;
                    string SourceSPCode = string.Empty;
                    string DestinationSPCode = string.Empty;
                    dr = dtExcelData.NewRow();
                    dr["SrNo"] = i + 1;
                    dr["SourceSPName"] = SPList[i];
                    var sourceSPInfo = dt_Source_SP.AsEnumerable().Where(x => x.Field<string>("SP_Name") == SPList[i]);
                    if (sourceSPInfo.AsDataView().Count > 0)
                    {
                        dr["SourceExist"] = "Yes";
                        SourceSPCode = sourceSPInfo.CopyToDataTable().Rows[0]["SP_Code"].ToString();
                        dr["SourceSPCode"] = SourceSPCode;
                        dr["SourceSPCreatedDate"] = sourceSPInfo.CopyToDataTable().Rows[0]["CREATED"].ToString(); ;
                        dr["SourceSPLastAlteredDate"] = sourceSPInfo.CopyToDataTable().Rows[0]["LAST_ALTERED"].ToString(); ;

                        var destinationSPInfo = dt_Destination_SP.AsEnumerable().Where(x => x.Field<string>("SP_Name") == SPList[i]);
                        if (destinationSPInfo.AsDataView().Count > 0)
                        {
                            dr["DestinationExist"] = "Yes";
                            DestinationSPCode = destinationSPInfo.CopyToDataTable().Rows[0]["SP_Code"].ToString();
                            dr["DestinationSPName"] = SPList[i];
                            dr["DestinationSPCode"] = DestinationSPCode;
                            dr["DestinationSPCreatedDate"] = destinationSPInfo.CopyToDataTable().Rows[0]["CREATED"].ToString(); ;
                            dr["DestinationSPLastAlteredDate"] = destinationSPInfo.CopyToDataTable().Rows[0]["LAST_ALTERED"].ToString(); ;

                            SourceSPCode = SourceSPCode.Replace("\n", String.Empty).Replace("\r", String.Empty).Replace("\t", String.Empty).Trim();
                            DestinationSPCode = DestinationSPCode.Replace("\n", String.Empty).Replace("\r", String.Empty).Replace("\t", String.Empty).Trim();
                            if (string.Equals(SourceSPCode, DestinationSPCode, StringComparison.OrdinalIgnoreCase))
                            {
                                dr["CodeMatch"] = "Yes";
                            }
                            else
                            {
                                dr["CodeMatch"] = "No";
                            }
                        }
                        else
                        {
                            dr["DestinationExist"] = "No";
                        }
                    }
                    else
                    {
                        dr["SourceExist"] = "No";
                    }
                    dtExcelData.Rows.Add(dr);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
