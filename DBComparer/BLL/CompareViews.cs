﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DBComparer.BLL
{
    public class CompareViews
    {
        string dt_Compare_View_List = string.Empty;
        DataTable dt_Source_View = new DataTable();
        DataTable dt_Destination_View = new DataTable();
        DataTable dtExcelData = new DataTable();
        DAL.DAL dal = new DAL.DAL();
        public CompareViews(string dt_Compare_View_List, DataTable dtExcelData)
        {
            this.dt_Compare_View_List = dt_Compare_View_List;
            this.dtExcelData = dtExcelData;
            dtExcelData.Columns.Add("SrNo");
            dtExcelData.Columns.Add("SourceViewName");
            dtExcelData.Columns.Add("SourceExist");
            dtExcelData.Columns.Add("DestinationViewName");
            dtExcelData.Columns.Add("DestinationExist");
        }

        public void Compare()
        {
            try
            {
                int j = 0;
                dt_Source_View = dal.GetViewList(dt_Compare_View_List, DAL.DAL.ConnectionType.Source);
                dt_Destination_View = dal.GetViewList(dt_Compare_View_List, DAL.DAL.ConnectionType.Destination);

                string[] ViewList = dt_Compare_View_List.Replace("'", "").Split(',');

                for (int i = 0; i < ViewList.Length; i++)
                {
                    DataRow dr = null;
                    dr = dtExcelData.NewRow();
                    dr["SrNo"] = i + 1;
                    dr["SourceViewName"] = ViewList[i];
                    var sourceViewInfo = dt_Source_View.AsEnumerable().Where(x => x.Field<string>("ViewName") == ViewList[i]);
                    if (sourceViewInfo.AsDataView().Count > 0)
                    {
                        dr["SourceExist"] = "Yes";
                        var destinationViewInfo = dt_Destination_View.AsEnumerable().Where(x => x.Field<string>("ViewName").Trim().ToLower() == ViewList[i].Trim().ToLower());
                        if (destinationViewInfo.AsDataView().Count > 0)
                        {
                            dr["DestinationExist"] = "Yes";
                        }
                        else
                        {
                            dr["DestinationExist"] = "No";
                        }
                    }
                    else
                    {
                        dr["SourceExist"] = "No";
                    }
                    dtExcelData.Rows.Add(dr);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
