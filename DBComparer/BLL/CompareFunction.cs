﻿using System;
using System.Data;

namespace DBComparer.BLL
{
    class CompareFunction
    {
        string dt_Compare_Function_List = string.Empty;
        DataTable dt_Source_Function = new DataTable();
        DataTable dt_Destination_Function = new DataTable();
        DataTable dtExcelData = new DataTable();
        DAL.DAL dal = new DAL.DAL();
        public CompareFunction(string dt_Compare_Function_List, DataTable dtExcelData)
        {
            this.dt_Compare_Function_List = dt_Compare_Function_List;
            this.dtExcelData = dtExcelData;
            dtExcelData.Columns.Add("SrNo");
            dtExcelData.Columns.Add("SourceFunctionName");
            dtExcelData.Columns.Add("SourceExist");
            dtExcelData.Columns.Add("SourceFunctionCode");
            dtExcelData.Columns.Add("SourceFunctionCreatedDate");
            dtExcelData.Columns.Add("SourceFunctionLastAlteredDate");
            dtExcelData.Columns.Add("DestinationExist");
            dtExcelData.Columns.Add("DestinationFunctionName");
            dtExcelData.Columns.Add("DestinationFunctionCode");
            dtExcelData.Columns.Add("DestinationFunctionCreatedDate");
            dtExcelData.Columns.Add("DestinationFunctionLastAlteredDate");
            dtExcelData.Columns.Add("CodeMatch");
        }
        public void Compare()
        {
            try
            {
                int j = 0;
                dt_Source_Function = dal.GetFunctionList(dt_Compare_Function_List, DAL.DAL.ConnectionType.Source);
                dt_Destination_Function = dal.GetFunctionList(dt_Compare_Function_List, DAL.DAL.ConnectionType.Destination);

                string[] FunctionList = dt_Compare_Function_List.Replace("'", "").Split(',');

                for (int i = 0; i < FunctionList.Length; i++)
                {
                    DataRow dr = null;
                    string SourceFunctionCode = string.Empty;
                    string DestinationFunctionCode = string.Empty;
                    dr = dtExcelData.NewRow();
                    dr["SrNo"] = i + 1;
                    dr["SourceFunctionName"] = FunctionList[i];
                    var sourceFunctionInfo = dt_Source_Function.AsEnumerable().Where(x => x.Field<string>("Function_Name") == FunctionList[i]);
                    if (sourceFunctionInfo.AsDataView().Count > 0)
                    {
                        dr["SourceExist"] = "Yes";
                        SourceFunctionCode = sourceFunctionInfo.CopyToDataTable().Rows[0]["Function_Code"].ToString();
                        dr["SourceFunctionCode"] = SourceFunctionCode;
                        dr["SourceFunctionCreatedDate"] = sourceFunctionInfo.CopyToDataTable().Rows[0]["CREATED"].ToString();
                        dr["SourceFunctionLastAlteredDate"] = sourceFunctionInfo.CopyToDataTable().Rows[0]["LAST_ALTERED"].ToString();

                        var destinationFunctionInfo = dt_Destination_Function.AsEnumerable().Where(x => x.Field<string>("Function_Name") == FunctionList[i]);
                        if (destinationFunctionInfo.AsDataView().Count > 0)
                        {
                            dr["DestinationExist"] = "Yes";
                            DestinationFunctionCode = destinationFunctionInfo.CopyToDataTable().Rows[0]["Function_Code"].ToString();
                            dr["DestinationFunctionName"] = FunctionList[i];
                            dr["DestinationFunctionCode"] = DestinationFunctionCode;
                            dr["DestinationFunctionCreatedDate"] = destinationFunctionInfo.CopyToDataTable().Rows[0]["CREATED"].ToString();
                            dr["DestinationFunctionLastAlteredDate"] = destinationFunctionInfo.CopyToDataTable().Rows[0]["LAST_ALTERED"].ToString();

                            SourceFunctionCode = SourceFunctionCode.Replace("\n", String.Empty).Replace("\r", String.Empty).Replace("\t", String.Empty).Trim();
                            DestinationFunctionCode = DestinationFunctionCode.Replace("\n", String.Empty).Replace("\r", String.Empty).Replace("\t", String.Empty).Trim();
                            if (string.Equals(SourceFunctionCode, DestinationFunctionCode, StringComparison.OrdinalIgnoreCase))
                            {
                                dr["CodeMatch"] = "Yes";
                            }
                            else
                            {
                                dr["CodeMatch"] = "No";
                            }
                        }
                        else
                        {
                            dr["DestinationExist"] = "No";
                        }
                    }
                    else
                    {
                        dr["SourceExist"] = "No";
                    }
                    dtExcelData.Rows.Add(dr);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
