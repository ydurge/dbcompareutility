﻿using System;
using System.Data;

namespace DBComparer.BLL
{
    public class CompareTables
    {
        string source_Compare_Tables_List = string.Empty;
        string destination_Compare_Tables_List = string.Empty;

        DataTable dt_Source_Tables = new DataTable();
        DataTable dt_Destination_Tables = new DataTable();

        DataTable dt_Source_Tables_ChangeTracking = new DataTable();
        DataTable dt_Destination_Tables_ChangeTracking = new DataTable();

        DataTable dtExcelData = new DataTable();
        DAL.DAL dal = new DAL.DAL();
        public CompareTables(string source_Compare_Tables_List, string destination_Compare_Tables_List, DataTable dtExcelData)
        {
            this.source_Compare_Tables_List = source_Compare_Tables_List;
            this.destination_Compare_Tables_List = destination_Compare_Tables_List;
            this.dtExcelData = dtExcelData;
            dtExcelData.Columns.Add("SrNo");
            dtExcelData.Columns.Add("SourceTableName");
            dtExcelData.Columns.Add("SourceExist");
            dtExcelData.Columns.Add("SourceRowsCount");
            dtExcelData.Columns.Add("SourceCTEEnabled");
            dtExcelData.Columns.Add("DestinationTableName");
            dtExcelData.Columns.Add("DestinationExist");
            dtExcelData.Columns.Add("DestinationRowsCount");
            dtExcelData.Columns.Add("DestinationCTEEnabled");
            dtExcelData.Columns.Add("CountMatch");
            dtExcelData.Columns.Add("CTEMatch");
        }
        public void Compare()
        {
            try
            {
                int j = 0;
                dt_Source_Tables = dal.GetTableList(source_Compare_Tables_List, DAL.DAL.ConnectionType.Source);
                dt_Destination_Tables = dal.GetTableList(destination_Compare_Tables_List, DAL.DAL.ConnectionType.Destination);
                dt_Source_Tables_ChangeTracking = dal.GetChangeTrackingTableList(DAL.DAL.ConnectionType.Source);
                dt_Destination_Tables_ChangeTracking = dal.GetChangeTrackingTableList(DAL.DAL.ConnectionType.Destination);

                string[] SourceTableList = source_Compare_Tables_List.Replace("'", "").Split(',');
                string[] DestinationTableList = source_Compare_Tables_List.Replace("'", "").Split(',');

                for (int i = 0; i < SourceTableList.Length; i++)
                {
                    DataRow dr = null;
                    Int64 SourceRowsCount = 0;
                    Int64 DestinationRowsCount = 0;
                    dr = dtExcelData.NewRow();
                    dr["SrNo"] = i + 1;
                    dr["SourceTableName"] = SourceTableList[i];
                    var sourceTableInfo = dt_Source_Tables.AsEnumerable().Where(x => x.Field<string>("TableName").Trim().ToLower() == SourceTableList[i].Trim().ToLower());
                    if (sourceTableInfo.AsDataView().Count > 0)
                    {
                        dr["SourceExist"] = "Yes";
                        SourceRowsCount = Convert.ToInt64(sourceTableInfo.CopyToDataTable().Rows[0]["RowsCount"].ToString());
                        dr["SourceRowsCount"] = SourceRowsCount;

                        ///Source CTE
                        var sourceTableCTE = dt_Source_Tables_ChangeTracking.AsEnumerable().Where(x => x.Field<string>("TableName").Trim().ToLower() == SourceTableList[i].Trim().ToLower());
                        bool IsSourceCTE = false;
                        bool IsDestinationCTE = false;
                        if (sourceTableCTE.AsDataView().Count > 0)
                        {
                            dr["SourceCTEEnabled"] = "Yes";
                            IsSourceCTE = true;
                        }
                        else
                        {
                            dr["SourceCTEEnabled"] = "No";
                        }
                        ///

                        var destinationTableInfo = dt_Destination_Tables.AsEnumerable().Where(x => x.Field<string>("TableName").Trim().ToLower() == DestinationTableList[i].Trim().ToLower());
                        if (destinationTableInfo.AsDataView().Count > 0)
                        {
                            dr["DestinationExist"] = "Yes";
                            dr["DestinationTableName"] = DestinationTableList[i];
                            DestinationRowsCount = Convert.ToInt64(destinationTableInfo.CopyToDataTable().Rows[0]["RowsCount"].ToString());
                            dr["DestinationRowsCount"] = DestinationRowsCount;

                            ///Destination CTE
                            var destinationTableCTE = dt_Destination_Tables_ChangeTracking.AsEnumerable().Where(x => x.Field<string>("TableName").Trim().ToLower() == DestinationTableList[i].Trim().ToLower());
                            if (destinationTableCTE.AsDataView().Count > 0)
                            {
                                dr["DestinationCTEEnabled"] = "Yes";
                                IsDestinationCTE = true;
                            }
                            else
                            {
                                dr["DestinationCTEEnabled"] = "No";
                            }
                            ///

                            if ((SourceRowsCount > 0 && DestinationRowsCount > 0) && (SourceRowsCount == DestinationRowsCount))
                            {
                                dr["CountMatch"] = "Yes";
                            }
                            else
                            {
                                dr["CountMatch"] = "No";
                            }

                            if (IsSourceCTE == IsDestinationCTE)
                            {
                                dr["CTEMatch"] = "Yes";
                            }
                            else
                            {
                                dr["CTEMatch"] = "No";
                            }
                        }
                        else
                        {
                            dr["DestinationExist"] = "No";
                        }
                    }
                    else
                    {
                        dr["SourceExist"] = "No";
                    }
                    dtExcelData.Rows.Add(dr);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
