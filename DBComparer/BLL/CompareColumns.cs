﻿using System;
using System.Data;

namespace DBComparer.BLL
{
    public class CompareColumns
    {
        string source_Compare_Tables_List = string.Empty;
        string destination_Compare_Tables_List = string.Empty;
        DataTable dt_Source_Tables = new DataTable();
        DataTable dt_Destination_Tables = new DataTable();
        DataTable dtExcelData = new DataTable();

        DAL.DAL dal = new DAL.DAL();
        public CompareColumns(string source_Compare_Tables_List, string destination_Compare_Tables_List, DataTable dtExcelData)
        {
            this.source_Compare_Tables_List = source_Compare_Tables_List;
            this.destination_Compare_Tables_List = destination_Compare_Tables_List;
            this.dtExcelData = dtExcelData;
            dtExcelData.Columns.Add("SrNo");
            dtExcelData.Columns.Add("SourceTableName");
            dtExcelData.Columns.Add("SourceExist");
            dtExcelData.Columns.Add("S.SrNo");
            dtExcelData.Columns.Add("SourceColumnName");
            dtExcelData.Columns.Add("SourceTypeName");
            dtExcelData.Columns.Add("SourceMaxLength");
            dtExcelData.Columns.Add("SourceIsNullable");
            dtExcelData.Columns.Add("SourceIsIdentity");

            //dtExcelData.Columns.Add("DestinationSrNo1");
            dtExcelData.Columns.Add("DestinationTableName");
            dtExcelData.Columns.Add("DestinationExist");
            dtExcelData.Columns.Add("D.SrNo");
            dtExcelData.Columns.Add("DestinationColumnName");
            dtExcelData.Columns.Add("DestinationTypeName");
            dtExcelData.Columns.Add("DestinationMaxLength");
            dtExcelData.Columns.Add("DestinationIsNullable");
            dtExcelData.Columns.Add("DestinationIsIdentity");

            dtExcelData.Columns.Add("Match_ColumnName");
            dtExcelData.Columns.Add("Match_TypeName");
            dtExcelData.Columns.Add("Match_MaxLegth");
            dtExcelData.Columns.Add("Match_IsNullable");
            dtExcelData.Columns.Add("Match_IsIdentity");
            dtExcelData.Columns.Add("Remarks");
        }
        public void Compare()
        {
            try
            {
                dt_Source_Tables = dal.GetTableList(source_Compare_Tables_List, DAL.DAL.ConnectionType.Source);
                dt_Destination_Tables = dal.GetTableList(destination_Compare_Tables_List, DAL.DAL.ConnectionType.Destination);

                string[] SourceTableList = source_Compare_Tables_List.Replace("'", "").Split(',');
                string[] DestinationTableList = destination_Compare_Tables_List.Replace("'", "").Split(',');

                for (int i = 0; i < SourceTableList.Length; i++)
                {
                    DataRow dr = null;
                    DataTable dt_Source_Columns = new DataTable();
                    int SourceColsCount = 0;
                    int RowsStart = dtExcelData.Rows.Count;
                    dr = dtExcelData.NewRow();
                    dr["SrNo"] = i + 1;
                    dr["SourceTableName"] = SourceTableList[i];
                    var sourceTableInfo = dt_Source_Tables.AsEnumerable().Where(x => x.Field<string>("TableName").Trim().ToLower() == SourceTableList[i].Trim().ToLower());
                    var destinationTableInfo = dt_Destination_Tables.AsEnumerable().Where(x => x.Field<string>("TableName").Trim().ToLower() == DestinationTableList[i].Trim().ToLower());

                    if (sourceTableInfo.AsDataView().Count > 0)
                    {
                        dr["SourceExist"] = "Yes";
                        dt_Source_Columns = dal.GetColumnList(Convert.ToString(sourceTableInfo.CopyToDataTable().Rows[0]["TableId"]), DAL.DAL.ConnectionType.Source);
                        SourceColsCount = dt_Source_Columns.Rows.Count;

                        DataTable dt_Destination_Columns = new DataTable();
                        int DestinationColsCount = 0;
                        if (destinationTableInfo.AsDataView().Count > 0)
                        {
                            //dr["DestinationSrNo1"] = i + 1;
                            dr["DestinationTableName"] = DestinationTableList[i];
                            dr["DestinationExist"] = "Yes";
                            dt_Destination_Columns = dal.GetColumnList(Convert.ToString(destinationTableInfo.CopyToDataTable().Rows[0]["TableId"]), DAL.DAL.ConnectionType.Destination);
                            DestinationColsCount = dt_Destination_Columns.Rows.Count;

                            // Column
                            if (SourceColsCount > 0)
                            {
                                for (int C = 0; C < dt_Source_Columns.Rows.Count; C++)
                                {
                                    dr["SrNo"] = i + 1;
                                    dr["SourceTableName"] = SourceTableList[i];

                                    dr["S.SrNo"] = C + 1;
                                    dr["SourceColumnName"] = Convert.ToString(dt_Source_Columns.Rows[C]["ColumnName"]);
                                    dr["SourceTypeName"] = Convert.ToString(dt_Source_Columns.Rows[C]["TypeName"]);
                                    dr["SourceMaxLength"] = Convert.ToString(dt_Source_Columns.Rows[C]["max_length"]);
                                    dr["SourceIsNullable"] = Convert.ToString(dt_Source_Columns.Rows[C]["is_nullable"]);
                                    dr["SourceIsIdentity"] = Convert.ToString(dt_Source_Columns.Rows[C]["is_identity"]);

                                    var destination_col_check = dt_Destination_Columns.AsEnumerable().Where(x => Convert.ToString(x.Field<string>("ColumnName")).Trim().ToLower() == Convert.ToString(dt_Source_Columns.Rows[C]["ColumnName"]).Trim().ToLower());
                                    if (destination_col_check.AsDataView().Count > 0)
                                    {
                                        DataTable destination_col_check_Table = destination_col_check.CopyToDataTable();
                                        dr["D.SrNo"] = C + 1;
                                        dr["DestinationColumnName"] = Convert.ToString(destination_col_check_Table.Rows[0]["ColumnName"]);
                                        dr["DestinationTypeName"] = Convert.ToString(destination_col_check_Table.Rows[0]["TypeName"]);
                                        dr["DestinationMaxLength"] = Convert.ToString(destination_col_check_Table.Rows[0]["max_length"]);
                                        dr["DestinationIsNullable"] = Convert.ToString(destination_col_check_Table.Rows[0]["is_nullable"]);
                                        dr["DestinationIsIdentity"] = Convert.ToString(destination_col_check_Table.Rows[0]["is_identity"]);

                                        if (Convert.ToString(dt_Source_Columns.Rows[C]["ColumnName"]).ToLower() == Convert.ToString(destination_col_check_Table.Rows[0]["ColumnName"]).ToLower())
                                        {
                                            dr["Match_ColumnName"] = "Yes";
                                        }
                                        else
                                        {
                                            dr["Match_ColumnName"] = "No";
                                        }

                                        if (Convert.ToString(dt_Source_Columns.Rows[C]["TypeName"]).ToLower() == Convert.ToString(destination_col_check_Table.Rows[0]["TypeName"]).ToLower())
                                        {
                                            dr["Match_TypeName"] = "Yes";
                                        }
                                        else
                                        {
                                            dr["Match_TypeName"] = "No";
                                        }

                                        if (Convert.ToString(dt_Source_Columns.Rows[C]["max_length"]).ToLower() == Convert.ToString(destination_col_check_Table.Rows[0]["max_length"]).ToLower())
                                        {
                                            dr["Match_MaxLegth"] = "Yes";
                                        }
                                        else
                                        {
                                            dr["Match_MaxLegth"] = "No";
                                        }

                                        if (Convert.ToString(dt_Source_Columns.Rows[C]["is_nullable"]).ToLower() == Convert.ToString(destination_col_check_Table.Rows[0]["is_nullable"]).ToLower())
                                        {
                                            dr["Match_IsNullable"] = "Yes";
                                        }
                                        else
                                        {
                                            dr["Match_IsNullable"] = "No";
                                        }

                                        if (Convert.ToString(dt_Source_Columns.Rows[C]["is_identity"]).ToLower() == Convert.ToString(destination_col_check_Table.Rows[0]["is_identity"]).ToLower())
                                        {
                                            dr["Match_IsIdentity"] = "Yes";
                                        }
                                        else
                                        {
                                            dr["Match_IsIdentity"] = "No";
                                        }
                                    }
                                    else
                                    {
                                        dr["Remarks"] = "Column not found";
                                    }

                                    dtExcelData.Rows.Add(dr);

                                    if (C + 1 < dt_Source_Columns.Rows.Count)
                                    {
                                        dr = dtExcelData.NewRow();
                                    }
                                }
                                dr = dtExcelData.NewRow();
                                if ((SourceColsCount > 0 && DestinationColsCount > 0) && (SourceColsCount != DestinationColsCount))
                                {
                                    dr["Remarks"] = "Columns count is mismatch";
                                }
                            }
                        }
                        else
                        {
                            dr["D.SrNo"] = i + 1;
                            dr["DestinationTableName"] = DestinationTableList[i];
                            dr["DestinationExist"] = "No";
                            dtExcelData.Rows.Add(dr);
                        }
                    }
                    else
                    {
                        dr["SourceExist"] = "No";
                        dtExcelData.Rows.Add(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}