﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using DBComparer.Helpers;
using System.IO;

namespace DBComparer
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        public Startup()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            Configuration = builder.Build();
        }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            //configuration["AWS.Logging:LogGroup"] = "batchextraction-" + Environment.GetEnvironmentVariable("EnvironmentName");
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(services);
            //services.Configure<Settings>(Configuration);
        }

    }
}
