﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace DBComparer.DAL
{
    public class DAL
    {
        public static IConfigurationRoot configuration;
        public enum ConnectionType
        {
            Source,
            Destination
        }
        public DAL()
        {
            ServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            // Build configuration
            configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
            .AddJsonFile("DBConnectionSetting.json", false)
            .Build();
        }

        public DataTable GetData(string SqlQuery)
        {
            DataTable dt = new DataTable();
            string connectionString = configuration.GetSection("AppDBConnectionString").Value.ToString();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    SqlCommand cmd = new SqlCommand(SqlQuery, connection);
                    cmd.CommandTimeout = 30;
                    dt.Load(cmd.ExecuteReader());
                }

            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        public DataTable GetTableList(string TableList, ConnectionType type)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            if (type == ConnectionType.Source)
            { connectionString = configuration.GetSection("SourceDBConnectionString").Value.ToString(); }
            else
            { connectionString = configuration.GetSection("DestinationDBConnectionString").Value.ToString(); }
            try
            {
                string SqlQuery = "SELECT " +
                                  "SCHEMA_NAME(schema_id) AS[SchemaName],  " +
                                  "max([Tables].[object_id]) as TableId, " +
                                  "[Tables].name AS[TableName], " +
                                  "SUM([Partitions].[rows]) AS[RowsCount] " +
                                  "FROM sys.tables AS[Tables] " +
                                  "JOIN sys.partitions AS[Partitions] " +
                                  "ON [Tables].[object_id] = [Partitions].[object_id] " +
                                  "AND[Partitions].index_id IN (0,1) " +
                                  $"WHERE[Tables].name in ({TableList})" +
                                  "GROUP BY SCHEMA_NAME(schema_id), [Tables].name";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    SqlCommand cmd = new SqlCommand(SqlQuery.ToString(), connection);
                    cmd.CommandTimeout = 30;
                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        public DataTable GetAllTableListWithCommaSeperator(ConnectionType type)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            if (type == ConnectionType.Source)
            { connectionString = configuration.GetSection("SourceDBConnectionString").Value.ToString(); }
            else
            { connectionString = configuration.GetSection("DestinationDBConnectionString").Value.ToString(); }
            try
            {
                string SqlQuery = "with TableList as ( " +
                                  "select STUFF((SELECT ''', ''' +[name] FROM sys.tables c FOR XML PATH(''), TYPE).value('.', 'nvarchar(max)'), 1, 1, '') as list " +
                                  ") select SUBSTRING(list,2,len(list))+'''' from TableList";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    SqlCommand cmd = new SqlCommand(SqlQuery.ToString(), connection);
                    cmd.CommandTimeout = 30;
                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        public DataTable GetAllViewListWithCommaSeperator(ConnectionType type)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            if (type == ConnectionType.Source)
            { connectionString = configuration.GetSection("SourceDBConnectionString").Value.ToString(); }
            else
            { connectionString = configuration.GetSection("DestinationDBConnectionString").Value.ToString(); }
            try
            {
                string SqlQuery = "with TableList as ( " +
                                  "select STUFF((SELECT ''', ''' +[name] FROM sys.Views c FOR XML PATH(''), TYPE).value('.', 'nvarchar(max)'), 1, 1, '') as list " +
                                  ") select SUBSTRING(list,2,len(list))+'''' from TableList";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    SqlCommand cmd = new SqlCommand(SqlQuery.ToString(), connection);
                    cmd.CommandTimeout = 30;
                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        public DataTable GetAllSPListWithCommaSeperator(ConnectionType type)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            if (type == ConnectionType.Source)
            { connectionString = configuration.GetSection("SourceDBConnectionString").Value.ToString(); }
            else
            { connectionString = configuration.GetSection("DestinationDBConnectionString").Value.ToString(); }
            try
            {
                string SqlQuery = "with SpList as ( " +
                                "select STUFF((SELECT ''', ''' +[ROUTINE_NAME] FROM information_schema.routines c  where routine_type = 'PROCEDURE' FOR XML PATH(''), TYPE).value('.', 'nvarchar(max)'), 1, 1, '') as list " +
                                ") select SUBSTRING(list,2,len(list))+'''' from SpList";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    SqlCommand cmd = new SqlCommand(SqlQuery.ToString(), connection);
                    cmd.CommandTimeout = 30;
                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        public DataTable GetAllFunctionListWithCommaSeperator(ConnectionType type)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            if (type == ConnectionType.Source)
            { connectionString = configuration.GetSection("SourceDBConnectionString").Value.ToString(); }
            else
            { connectionString = configuration.GetSection("DestinationDBConnectionString").Value.ToString(); }
            try
            {
                string SqlQuery = "with FunctionList as ( " +
                                "select STUFF((SELECT ''', ''' +[ROUTINE_NAME] FROM information_schema.routines c  where routine_type = 'FUNCTION' FOR XML PATH(''), TYPE).value('.', 'nvarchar(max)'), 1, 1, '') as list " +
                                ") select SUBSTRING(list,2,len(list))+'''' from FunctionList";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    SqlCommand cmd = new SqlCommand(SqlQuery.ToString(), connection);
                    cmd.CommandTimeout = 30;
                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        public DataTable GetChangeTrackingTableList(ConnectionType type)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            if (type == ConnectionType.Source)
            { connectionString = configuration.GetSection("SourceDBConnectionString").Value.ToString(); }
            else
            { connectionString = configuration.GetSection("DestinationDBConnectionString").Value.ToString(); }
            try
            {
                string SqlQuery = "SELECT s.name as Schema_name, t.name AS TableName " +
                                  "FROM sys.change_tracking_tables tr " +
                                  "INNER JOIN sys.tables t on t.object_id = tr.object_id " +
                                  "INNER JOIN sys.schemas s on s.schema_id = t.schema_id";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    SqlCommand cmd = new SqlCommand(SqlQuery.ToString(), connection);
                    cmd.CommandTimeout = 30;
                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        public DataTable GetViewList(string ViewList, ConnectionType type)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            if (type == ConnectionType.Source)
            { connectionString = configuration.GetSection("SourceDBConnectionString").Value.ToString(); }
            else
            { connectionString = configuration.GetSection("DestinationDBConnectionString").Value.ToString(); }
            try
            {
                string SqlQuery = "select [object_id],[name] as [ViewName] from sys.views where [name] in(" + ViewList + ")";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    SqlCommand cmd = new SqlCommand(SqlQuery.ToString(), connection);
                    cmd.CommandTimeout = 30;
                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        public DataTable GetSPList(string SPList, ConnectionType type)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            if (type == ConnectionType.Source)
            { connectionString = configuration.GetSection("SourceDBConnectionString").Value.ToString(); }
            else
            { connectionString = configuration.GetSection("DestinationDBConnectionString").Value.ToString(); }
            try
            {
                string SqlQuery = "select ROUTINE_NAME as [SP_Name], ROUTINE_DEFINITION as [SP_Code], CREATED, LAST_ALTERED from information_schema.routines where routine_type = 'PROCEDURE' " +
                                  "and ROUTINE_NAME in(" + SPList + ")";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    SqlCommand cmd = new SqlCommand(SqlQuery.ToString(), connection);
                    cmd.CommandTimeout = 30;
                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        public DataTable GetFunctionList(string FunctionList, ConnectionType type)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            if (type == ConnectionType.Source)
            { connectionString = configuration.GetSection("SourceDBConnectionString").Value.ToString(); }
            else
            { connectionString = configuration.GetSection("DestinationDBConnectionString").Value.ToString(); }
            try
            {
                string SqlQuery = "select ROUTINE_NAME as [Function_Name], ROUTINE_DEFINITION as [Function_Code], CREATED, LAST_ALTERED from information_schema.routines where routine_type = 'FUNCTION' " +
                                  "and ROUTINE_NAME in(" + FunctionList + ")";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    SqlCommand cmd = new SqlCommand(SqlQuery.ToString(), connection);
                    cmd.CommandTimeout = 30;
                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        public DataTable GetColumnList(string TableId, ConnectionType type)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            if (type == ConnectionType.Source)
            { connectionString = configuration.GetSection("SourceDBConnectionString").Value.ToString(); }
            else
            { connectionString = configuration.GetSection("DestinationDBConnectionString").Value.ToString(); }
            try
            {
                string SqlQuery = "Select A.[object_id] as TableId, A.[name] as TableName, B.column_id as ColumnId, B.[name] as ColumnName, C.system_type_id as [TypeId], C.[name] as [TypeName], B.max_length, B.is_nullable, B.is_identity from sys.tables as A " +
                                "inner join sys.columns as B on A.object_id = B.object_id " +
                                "inner join sys.types as C on B.system_type_id = C.system_type_id " +
                                "where C.[name]<>'sysname' and A.[object_id]=" + TableId;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    SqlCommand cmd = new SqlCommand(SqlQuery.ToString(), connection);
                    cmd.CommandTimeout = 30;
                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }

        public DataTable GetTableIndexList(string TableId, ConnectionType type)
        {
            DataTable dt = new DataTable();
            string connectionString = string.Empty;
            if (type == ConnectionType.Source)
            { connectionString = configuration.GetSection("SourceDBConnectionString").Value.ToString(); }
            else
            { connectionString = configuration.GetSection("DestinationDBConnectionString").Value.ToString(); }
            try
            {
                string SqlQuery = "With table_index as( " +
                "SELECT '[' + s.NAME + '].[' + o.NAME + ']' AS 'table_name' " +
                ",+ i.NAME AS 'index_name' " +
                ",LOWER(i.type_desc) + CASE  " +
                "WHEN i.is_unique = 1 " +
                "THEN ', unique' " +
                "ELSE '' " +
                "END + CASE  " +
                "WHEN i.is_primary_key = 1 " +
                "THEN ', primary key' " +
                "ELSE '' " +
                "END AS 'index_description' " +
                ",STUFF(( " +
                "SELECT ', [' + sc.NAME + ']' AS \"text()\" " +
                "FROM syscolumns AS sc " +
                "INNER JOIN sys.index_columns AS ic ON ic.object_id = sc.id " +
                "AND ic.column_id = sc.colid " +
                "WHERE sc.id = so.object_id " +
                "AND ic.index_id = i1.indid " +
                "AND ic.is_included_column = 0 " +
                "ORDER BY key_ordinal " +
                "FOR XML PATH('') " +
                "), 1, 2, '') AS 'indexed_columns' " +
                ",STUFF(( " +
                "SELECT ', [' + sc.NAME + ']' AS \"text()\" " +
                "FROM syscolumns AS sc " +
                "INNER JOIN sys.index_columns AS ic ON ic.object_id = sc.id " +
                "AND ic.column_id = sc.colid " +
                "WHERE sc.id = so.object_id " +
                "AND ic.index_id = i1.indid " +
                "AND ic.is_included_column = 1 " +
                "FOR XML PATH('') " +
                "), 1, 2, '') AS 'included_columns' " +
                "FROM sysindexes AS i1 " +
                "INNER JOIN sys.indexes AS i ON i.object_id = i1.id " +
                "AND i.index_id = i1.indid " +
                "INNER JOIN sysobjects AS o ON o.id = i1.id " +
                "INNER JOIN sys.objects AS so ON so.object_id = o.id " +
                "AND is_ms_shipped = 0 " +
                "INNER JOIN sys.schemas AS s ON s.schema_id = so.schema_id " +
                "WHERE so.type = 'U' " +
                "AND i1.indid < 255 " +
                "AND i1.STATUS & 64 = 0 " +
                "AND i1.STATUS & 8388608 = 0 " +
                "AND i1.STATUS & 16777216 = 0 " +
                "AND i.type_desc <> 'heap' " +
                "AND so.NAME <> 'sysdiagrams' " +
                "and o.id=" + TableId + ") " +
                "Select table_name, index_name, index_description, indexed_columns, isnull(included_columns, '') as included_columns from table_index ORDER BY index_description, indexed_columns,included_columns";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    SqlCommand cmd = new SqlCommand(SqlQuery.ToString(), connection);
                    cmd.CommandTimeout = 30;
                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }
    }
}
