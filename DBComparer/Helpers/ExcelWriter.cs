﻿using OfficeOpenXml;
using System;
using System.Data;
using System.Drawing;
using System.IO;
// https://stackoverflow.com/questions/40487670/excel-nuget-package-for-net-core

namespace DBComparer.Helpers
{
    public class ExcelWriter
    {
        public static void CreateExcelFile(DataTable dtExcelData_CompareTables, DataTable dtExcelData_CompareViews, DataTable dtExcelData_CompareColumns, DataTable dtExcelData_CompareTableIndexes, DataTable dtExcelData_CompareSps, DataTable dtExcelData_CompareFunctions)
        {
            try
            {
                using (var package = new ExcelPackage())
                {
                    Write_Table(package, dtExcelData_CompareTables);
                    Write_View(package, dtExcelData_CompareViews);
                    Write_Column(package, dtExcelData_CompareColumns);
                    Write_Index(package, dtExcelData_CompareTableIndexes);
                    Write_SP(package, dtExcelData_CompareSps);
                    Write_Function(package, dtExcelData_CompareFunctions);
                    FileInfo file = new FileInfo(DateTime.Now.ToString("yyyy-MMM-dd  HH-mm-ss") + ".xlsx");
                    package.SaveAs(file);
                    Console.WriteLine("Excel report is generated with name [" + file.Name + "]");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static void Write_Table(ExcelPackage package, DataTable dtExcelData)
        {
            //dtExcelData.Columns.Add("SrNo");
            //dtExcelData.Columns.Add("SourceTableName");
            //dtExcelData.Columns.Add("SourceExist");
            //dtExcelData.Columns.Add("SourceRowsCount");
            //dtExcelData.Columns.Add("DestinationTableName");
            //dtExcelData.Columns.Add("DestinationExist");
            //dtExcelData.Columns.Add("DestinationRowsCount");
            //dtExcelData.Columns.Add("CountMatch");

            //  0dtExcelData.Columns.Add("SrNo");
            //1dtExcelData.Columns.Add("SourceTableName");
            //2dtExcelData.Columns.Add("SourceExist");
            //  3dtExcelData.Columns.Add("SourceRowsCount");
            //4dtExcelData.Columns.Add("SourceCTEEnabled");
            //5dtExcelData.Columns.Add("DestinationTableName");
            //6dtExcelData.Columns.Add("DestinationExist");
            //  7dtExcelData.Columns.Add("DestinationRowsCount");
            //8dtExcelData.Columns.Add("DestinationCTEEnabled");
            //9dtExcelData.Columns.Add("CountMatch");
            //10dtExcelData.Columns.Add("CTEMatch");

            try
            {
                if (dtExcelData.Columns.Count > 0)
                {
                    ExcelWorksheet Tableworksheet = package.Workbook.Worksheets.Add("Table");
                    for (int i = 0; i < dtExcelData.Columns.Count; i++)
                    {
                        Tableworksheet.Cells[1, i + 1].Value = dtExcelData.Columns[i].ColumnName;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Bold = true;
                        Tableworksheet.Cells[1, i + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
                        Tableworksheet.Cells[1, i + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Size = 13;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Color.SetColor(Color.Blue);
                    }
                    for (int i = 0; i < dtExcelData.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtExcelData.Columns.Count; j++)
                        {
                            //Tableworksheet.Cells[i + 2, j + 1].Value = dtExcelData.Rows[i][j].ToString();
                            if (j == 0 || j == 3 || j == 7)
                            {
                                if (!string.IsNullOrWhiteSpace(dtExcelData.Rows[i][j].ToString()))
                                {
                                    Tableworksheet.Cells[i + 2, j + 1].Value = long.Parse(dtExcelData.Rows[i][j].ToString());
                                    Tableworksheet.Cells[i + 2, j + 1].Style.Numberformat.Format = "0";
                                }
                            }
                            else
                            {
                                Tableworksheet.Cells[i + 2, j + 1].Value = dtExcelData.Rows[i][j].ToString();
                            }
                            if (dtExcelData.Rows[i][j].ToString().Trim().ToLower().Equals("no"))
                            {
                                Tableworksheet.Cells[i + 2, j + 1].Style.Font.Bold = true;
                                Tableworksheet.Cells[i + 2, j + 1].Style.Font.Color.SetColor(Color.Red);
                            }
                            Tableworksheet.Cells[i + 2, j + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                            //Tableworksheet.Cells.AutoFitColumns(0);
                        }
                    }
                    Tableworksheet.Cells.AutoFitColumns(0);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static void Write_View(ExcelPackage package, DataTable dtExcelData)
        {
            //dtExcelData.Columns.Add("SrNo");
            //dtExcelData.Columns.Add("SourceViewName");
            //dtExcelData.Columns.Add("SourceExist");
            //dtExcelData.Columns.Add("DestinationViewName");
            //dtExcelData.Columns.Add("DestinationExist");
            try
            {
                if (dtExcelData.Columns.Count > 0)
                {
                    ExcelWorksheet Tableworksheet = package.Workbook.Worksheets.Add("View");
                    for (int i = 0; i < dtExcelData.Columns.Count; i++)
                    {
                        Tableworksheet.Cells[1, i + 1].Value = dtExcelData.Columns[i].ColumnName;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Bold = true;
                        Tableworksheet.Cells[1, i + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
                        Tableworksheet.Cells[1, i + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Size = 13;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Color.SetColor(Color.Blue);
                    }
                    for (int i = 0; i < dtExcelData.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtExcelData.Columns.Count; j++)
                        {
                            if (j == 0)
                            {
                                if (!string.IsNullOrWhiteSpace(dtExcelData.Rows[i][j].ToString()))
                                {
                                    Tableworksheet.Cells[i + 2, j + 1].Value = long.Parse(dtExcelData.Rows[i][j].ToString());
                                    Tableworksheet.Cells[i + 2, j + 1].Style.Numberformat.Format = "0";
                                }
                            }
                            else
                            {
                                Tableworksheet.Cells[i + 2, j + 1].Value = dtExcelData.Rows[i][j].ToString();
                            }
                            if (dtExcelData.Rows[i][j].ToString().Trim().ToLower().Equals("no"))
                            {
                                Tableworksheet.Cells[i + 2, j + 1].Style.Font.Bold = true;
                                Tableworksheet.Cells[i + 2, j + 1].Style.Font.Color.SetColor(Color.Red);
                            }
                            Tableworksheet.Cells[i + 2, j + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        }
                        Tableworksheet.Cells.AutoFitColumns(0);
                    }
                    Tableworksheet.Cells.AutoFitColumns(0);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static void Write_Column(ExcelPackage package, DataTable dtExcelData)
        {
            //dtExcelData.Columns.Add("SourceSrNo1");
            //dtExcelData.Columns.Add("SourceTableName");
            //dtExcelData.Columns.Add("SourceExist");
            //dtExcelData.Columns.Add("SourceSrNo2");
            //dtExcelData.Columns.Add("SourceColumnName");
            //dtExcelData.Columns.Add("SourceTypeName");
            //dtExcelData.Columns.Add("SourceMaxLength");
            //dtExcelData.Columns.Add("SourceIsNullable");
            //dtExcelData.Columns.Add("SourceIsIdentity");

            //dtExcelData.Columns.Add("DestinationSrNo1");
            //dtExcelData.Columns.Add("DestinationTableName");
            //dtExcelData.Columns.Add("DestinationExist");
            //dtExcelData.Columns.Add("DestinationSrNo2");
            //dtExcelData.Columns.Add("DestinationColumnName");
            //dtExcelData.Columns.Add("DestinationTypeName");
            //dtExcelData.Columns.Add("DestinationMaxLength");
            //dtExcelData.Columns.Add("DestinationIsNullable");
            //dtExcelData.Columns.Add("DestinationIsIdentity");

            //dtExcelData.Columns.Add("Match_ColumnName");
            //dtExcelData.Columns.Add("Match_TypeName");
            //dtExcelData.Columns.Add("Match_MaxLegth");
            //dtExcelData.Columns.Add("Match_IsNullable");
            //dtExcelData.Columns.Add("Match_IsIdentity");
            //dtExcelData.Columns.Add("Remarks");
            try
            {
                if (dtExcelData.Columns.Count > 0)
                {
                    ExcelWorksheet Tableworksheet = package.Workbook.Worksheets.Add("Column");
                    for (int i = 0; i < dtExcelData.Columns.Count; i++)
                    {
                        Tableworksheet.Cells[1, i + 1].Value = dtExcelData.Columns[i].ColumnName;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Bold = true;
                        Tableworksheet.Cells[1, i + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
                        Tableworksheet.Cells[1, i + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Size = 13;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Color.SetColor(Color.Blue);
                    }
                    for (int i = 0; i < dtExcelData.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtExcelData.Columns.Count; j++)
                        {
                            if (j == 0 || j == 3 || j == 6 || j == 11 || j == 14)
                            {
                                if (!string.IsNullOrWhiteSpace(dtExcelData.Rows[i][j].ToString()))
                                {
                                    Tableworksheet.Cells[i + 2, j + 1].Value = long.Parse(dtExcelData.Rows[i][j].ToString());
                                    Tableworksheet.Cells[i + 2, j + 1].Style.Numberformat.Format = "0";
                                }
                            }
                            else
                            {
                                Tableworksheet.Cells[i + 2, j + 1].Value = dtExcelData.Rows[i][j].ToString();
                            }
                            if (dtExcelData.Rows[i][j].ToString().Trim().ToLower().Equals("no"))
                            {
                                Tableworksheet.Cells[i + 2, j + 1].Style.Font.Bold = true;
                                Tableworksheet.Cells[i + 2, j + 1].Style.Font.Color.SetColor(Color.Red);
                            }
                            Tableworksheet.Cells[i + 2, j + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        }
                        //Tableworksheet.Cells.AutoFitColumns(0);
                    }
                    Tableworksheet.Cells.AutoFitColumns(0);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static void Write_Index(ExcelPackage package, DataTable dtExcelData)
        {
            //dtExcelData.Columns.Add("SrNo");
            //dtExcelData.Columns.Add("SourceTableName");
            //dtExcelData.Columns.Add("SourceExist");
            //dtExcelData.Columns.Add("SourceSrNo");
            //dtExcelData.Columns.Add("SourceIndexType");
            //dtExcelData.Columns.Add("SourceIndexName");
            //dtExcelData.Columns.Add("SourceIndexedColumns");
            //dtExcelData.Columns.Add("SourceIncludedColumns");
            //dtExcelData.Columns.Add("DestinationTableName");
            //dtExcelData.Columns.Add("DestinationExist");
            //dtExcelData.Columns.Add("DestinationIndexExist");
            //dtExcelData.Columns.Add("DestinationSrNo");
            //dtExcelData.Columns.Add("DestinationIndexType");
            //dtExcelData.Columns.Add("DestinationIndexName");
            //dtExcelData.Columns.Add("DestinationIndexedColumns");
            //dtExcelData.Columns.Add("DestinationIncludedColumns");
            //dtExcelData.Columns.Add("Remarks");
            try
            {
                if (dtExcelData.Columns.Count > 0)
                {
                    ExcelWorksheet Tableworksheet = package.Workbook.Worksheets.Add("Index");
                    for (int i = 0; i < dtExcelData.Columns.Count; i++)
                    {
                        Tableworksheet.Cells[1, i + 1].Value = dtExcelData.Columns[i].ColumnName;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Bold = true;
                        Tableworksheet.Cells[1, i + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
                        Tableworksheet.Cells[1, i + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Size = 13;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Color.SetColor(Color.Blue);
                    }
                    for (int i = 0; i < dtExcelData.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtExcelData.Columns.Count; j++)
                        {
                            if (j == 0 || j == 3 || j == 11)
                            {
                                if (!string.IsNullOrWhiteSpace(dtExcelData.Rows[i][j].ToString()))
                                {
                                    Tableworksheet.Cells[i + 2, j + 1].Value = long.Parse(dtExcelData.Rows[i][j].ToString());
                                    Tableworksheet.Cells[i + 2, j + 1].Style.Numberformat.Format = "0";
                                }
                            }
                            else if (j == 16)
                            {
                                if (!string.IsNullOrWhiteSpace(dtExcelData.Rows[i][j].ToString()))
                                {
                                    Tableworksheet.Cells[i + 2, j + 1].Value = dtExcelData.Rows[i][j].ToString();
                                    Tableworksheet.Cells[i + 2, j + 1].Style.Font.Bold = true;
                                    Tableworksheet.Cells[i + 2, j + 1].Style.Font.Color.SetColor(Color.Red);
                                }
                            }
                            else
                            {
                                Tableworksheet.Cells[i + 2, j + 1].Value = dtExcelData.Rows[i][j].ToString();
                            }
                            if (dtExcelData.Rows[i][j].ToString().Trim().ToLower().Equals("no"))
                            {
                                Tableworksheet.Cells[i + 2, j + 1].Style.Font.Bold = true;
                                Tableworksheet.Cells[i + 2, j + 1].Style.Font.Color.SetColor(Color.Red);
                            }
                            Tableworksheet.Cells[i + 2, j + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        }
                        //Tableworksheet.Cells.AutoFitColumns(0);
                    }
                    Tableworksheet.Cells.AutoFitColumns(0);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static void Write_SP(ExcelPackage package, DataTable dtExcelData)
        {
            //dtExcelData.Columns.Add("SrNo");
            //dtExcelData.Columns.Add("SourceSPName");
            //dtExcelData.Columns.Add("SourceExist");
            //dtExcelData.Columns.Add("SourceSPCode");
            //dtExcelData.Columns.Add("SourceSPCreatedDate");
            //dtExcelData.Columns.Add("SourceSPLastAlteredDate");
            //dtExcelData.Columns.Add("DestinationExist");
            //dtExcelData.Columns.Add("DestinationSPName");
            //dtExcelData.Columns.Add("DestinationSPCode");
            //dtExcelData.Columns.Add("DestinationSPCreatedDate");
            //dtExcelData.Columns.Add("DestinationSPLastAlteredDate");
            //dtExcelData.Columns.Add("CodeMatch");
            try
            {
                if (dtExcelData.Columns.Count > 0)
                {
                    ExcelWorksheet Tableworksheet = package.Workbook.Worksheets.Add("SP");
                    for (int i = 0; i < dtExcelData.Columns.Count; i++)
                    {
                        Tableworksheet.Cells[1, i + 1].Value = dtExcelData.Columns[i].ColumnName;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Bold = true;
                        Tableworksheet.Cells[1, i + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
                        Tableworksheet.Cells[1, i + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        Tableworksheet.Cells[1, i + 1].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Size = 13;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Color.SetColor(Color.Blue);
                    }
                    for (int i = 0; i < dtExcelData.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtExcelData.Columns.Count; j++)
                        {
                            if (j == 0)
                            {
                                if (!string.IsNullOrWhiteSpace(dtExcelData.Rows[i][j].ToString()))
                                {
                                    Tableworksheet.Cells[i + 2, j + 1].Value = long.Parse(dtExcelData.Rows[i][j].ToString());
                                    Tableworksheet.Cells[i + 2, j + 1].Style.Numberformat.Format = "0";
                                    Tableworksheet.Cells[1, i + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    Tableworksheet.Cells[1, i + 1].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                }
                            }
                            else
                            {
                                Tableworksheet.Cells[i + 2, j + 1].Value = dtExcelData.Rows[i][j].ToString();
                            }
                            if (dtExcelData.Rows[i][j].ToString().Trim().ToLower().Equals("no"))
                            {
                                Tableworksheet.Cells[i + 2, j + 1].Style.Font.Bold = true;
                                Tableworksheet.Cells[i + 2, j + 1].Style.Font.Color.SetColor(Color.Red);
                            }
                        }
                        //Tableworksheet.Cells.AutoFitColumns(20);
                    }
                    Tableworksheet.Cells.AutoFitColumns(0);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        private static void Write_Function(ExcelPackage package, DataTable dtExcelData)
        {
            //dtExcelData.Columns.Add("SrNo");
            //dtExcelData.Columns.Add("SourceSPName");
            //dtExcelData.Columns.Add("SourceExist");
            //dtExcelData.Columns.Add("SourceSPCode");
            //dtExcelData.Columns.Add("SourceSPCreatedDate");
            //dtExcelData.Columns.Add("SourceSPLastAlteredDate");
            //dtExcelData.Columns.Add("DestinationExist");
            //dtExcelData.Columns.Add("DestinationSPName");
            //dtExcelData.Columns.Add("DestinationSPCode");
            //dtExcelData.Columns.Add("DestinationSPCreatedDate");
            //dtExcelData.Columns.Add("DestinationSPLastAlteredDate");
            //dtExcelData.Columns.Add("CodeMatch");
            try
            {
                if (dtExcelData.Columns.Count > 0)
                {
                    ExcelWorksheet Tableworksheet = package.Workbook.Worksheets.Add("Function");
                    for (int i = 0; i < dtExcelData.Columns.Count; i++)
                    {
                        Tableworksheet.Cells[1, i + 1].Value = dtExcelData.Columns[i].ColumnName;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Bold = true;
                        Tableworksheet.Cells[1, i + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
                        Tableworksheet.Cells[1, i + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        Tableworksheet.Cells[1, i + 1].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Size = 13;
                        Tableworksheet.Cells[1, i + 1].Style.Font.Color.SetColor(Color.Blue);
                    }
                    for (int i = 0; i < dtExcelData.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtExcelData.Columns.Count; j++)
                        {
                            if (j == 0)
                            {
                                if (!string.IsNullOrWhiteSpace(dtExcelData.Rows[i][j].ToString()))
                                {
                                    Tableworksheet.Cells[i + 2, j + 1].Value = long.Parse(dtExcelData.Rows[i][j].ToString());
                                    Tableworksheet.Cells[i + 2, j + 1].Style.Numberformat.Format = "0";
                                    Tableworksheet.Cells[1, i + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    Tableworksheet.Cells[1, i + 1].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                }
                            }
                            else
                            {
                                Tableworksheet.Cells[i + 2, j + 1].Value = dtExcelData.Rows[i][j].ToString();
                            }
                            if (dtExcelData.Rows[i][j].ToString().Trim().ToLower().Equals("no"))
                            {
                                Tableworksheet.Cells[i + 2, j + 1].Style.Font.Bold = true;
                                Tableworksheet.Cells[i + 2, j + 1].Style.Font.Color.SetColor(Color.Red);
                            }
                        }
                        //Tableworksheet.Cells.AutoFitColumns(20);
                    }
                    Tableworksheet.Cells.AutoFitColumns(0);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


    }
}
