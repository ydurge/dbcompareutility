﻿/////////////////////////////////////////////////////////////
//@Developer: Yash Durge
//@Development StartDate: 24 July 2019
/////////////////////////////////////////////////////////////
using DBComparer.BLL;
using DBComparer.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Data;
using System.IO;

namespace DBComparer
{
    class Program
    {
        public static IConfigurationRoot configuration_AppSettings;
        public static IConfigurationRoot configuration_DBConnectionSettings;
        static void Main(string[] args)
        {

            DAL.DAL dal = new DAL.DAL();

            ////////////////////////////////////////////////////////////////////
            // Read AppSetting.json
            ////////////////////////////////////////////////////////////////////
            ServiceCollection serviceCollection_DBConnectionSettings = new ServiceCollection();
            configuration_DBConnectionSettings = new ConfigurationBuilder()
            .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
            .AddJsonFile("DBConnectionSetting.json", false)
            .Build();
            string SourceDBConnection = configuration_DBConnectionSettings.GetSection("SourceDBConnectionString").Value;
            string TargetDBConnection = configuration_DBConnectionSettings.GetSection("DestinationDBConnectionString").Value;
            ////////////////////////////////////////////////////////////////////

            Console.WriteLine("*******************************************************************************************");
            Console.WriteLine("Source Database: " + SourceDBConnection.Split(';')[0]);
            Console.WriteLine(SourceDBConnection.Split(';')[1]);
            Console.WriteLine("Destination Database: " + TargetDBConnection.Split(';')[0]);
            Console.WriteLine(TargetDBConnection.Split(';')[1]);
            Console.WriteLine("*******************************************************************************************");


            ////////////////////////////////////////////////////////////////////
            // Read AppSetting.json
            ////////////////////////////////////////////////////////////////////
            ServiceCollection serviceCollection_AppSettings = new ServiceCollection();
            configuration_AppSettings = new ConfigurationBuilder()
            .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
            .AddJsonFile("AppSetting.json", false)
            .Build();
            bool IsTableCompare = bool.Parse(configuration_AppSettings.GetSection("IsTableCompare").Value);
            bool IsViewCompare = bool.Parse(configuration_AppSettings.GetSection("IsViewCompare").Value);
            bool IsColumnCompare = bool.Parse(configuration_AppSettings.GetSection("IsColumnCompare").Value);
            bool IsIndexCompare = bool.Parse(configuration_AppSettings.GetSection("IsIndexCompare").Value);
            bool IsSPCompare = bool.Parse(configuration_AppSettings.GetSection("IsSPCompare").Value);
            bool IsFunctionCompare = bool.Parse(configuration_AppSettings.GetSection("IsFunctionCompare").Value);

            string SourceTableList = configuration_AppSettings.GetSection("SourceTableList").Value;
            string DestinationTableList = configuration_AppSettings.GetSection("DestinationTableList").Value;
            string ViewList = configuration_AppSettings.GetSection("ViewList").Value;
            string StoreProcedureList = configuration_AppSettings.GetSection("StoreProcedureList").Value;
            string FunctionList = configuration_AppSettings.GetSection("FunctionList").Value;
            ////////////////////////////////////////////////////////////////////


            ////////////////////////////////////////////////////////////////////
            // Table Lists - Checking
            ////////////////////////////////////////////////////////////////////
            string List_Source_Tables = string.Empty;
            string List_Destination_Tables = string.Empty;
            
            if (SourceTableList.Trim().Length > 0)
            {
                List_Source_Tables = "'" + SourceTableList.Replace(",", "','") + "'";
            }
            else
            {
                List_Source_Tables = Convert.ToString(dal.GetAllTableListWithCommaSeperator(DAL.DAL.ConnectionType.Source).Rows[0][0]);
                List_Destination_Tables = List_Source_Tables;
            }

            if (DestinationTableList.Trim().Length > 0)
            {
                List_Destination_Tables = "'" + SourceTableList.Replace(",", "','") + "'";
            }

            long source_table_count = List_Source_Tables.Split(',').Length;
            long destination_table_count = List_Destination_Tables.Split(',').Length;
            if (source_table_count != destination_table_count)
            {
                Console.WriteLine("Error : Source and Destination table list count is not match, Check AppSettings.json");
            }
            Console.WriteLine("Source Tables Count: " + source_table_count);
            Console.WriteLine("Destination Tables Count: " + destination_table_count);
            ////////////////////////////////////////////////////////////////////
            // View Lists - Checking
            ////////////////////////////////////////////////////////////////////
            string List_View = string.Empty;
            if (List_View.Trim().Length > 0)
            {
                List_View = "'" + ViewList.Replace(",", "','") + "'";
            }
            else
            {
                List_View = Convert.ToString(dal.GetAllViewListWithCommaSeperator(DAL.DAL.ConnectionType.Source).Rows[0][0]);
            }
            Console.WriteLine("Views Count: " + List_View.Split(',').Length);
            ////////////////////////////////////////////////////////////////////
            // Store Procedure Lists - Checking
            ////////////////////////////////////////////////////////////////////
            string List_SP = string.Empty;
            if (List_SP.Trim().Length > 0)
            {
                List_SP = "'" + StoreProcedureList.Replace(",", "','") + "'";
            }
            else
            {
                List_SP = Convert.ToString(dal.GetAllSPListWithCommaSeperator(DAL.DAL.ConnectionType.Source).Rows[0][0]);
            }
            Console.WriteLine("SP Count: " + List_SP.Split(',').Length);
            ////////////////////////////////////////////////////////////////////
            // Function Lists - Checking
            ////////////////////////////////////////////////////////////////////
            string List_Function = string.Empty;
            if (List_Function.Trim().Length > 0)
            {
                List_Function = "'" + FunctionList.Replace(",", "','") + "'";
            }
            else
            {
                List_Function = Convert.ToString(dal.GetAllFunctionListWithCommaSeperator(DAL.DAL.ConnectionType.Source).Rows[0][0]);
            }
            Console.WriteLine("Function Count: " + List_Function.Split(',').Length);
            ////////////////////////////////////////////////////////////////////
            Console.WriteLine("*******************************************************************************************");

            //For Excel
            DataTable dtExcelData_CompareTables = new DataTable();
            DataTable dtExcelData_CompareViews = new DataTable();
            DataTable dtExcelData_CompareColumns = new DataTable();
            DataTable dtExcelData_CompareTableIndexes = new DataTable();
            DataTable dtExcelData_CompareSps = new DataTable();
            DataTable dtExcelData_CompareFunctions = new DataTable();

            //DataTable dt_Compare_Tables = new DataTable();

            //string source_Compare_Tables_List = string.Empty;
            //string destination_Compare_Tables_List = string.Empty;
            //string dt_Compare_View_List = string.Empty;
            //string dt_Compare_SP_List = string.Empty;
            //string dt_Compare_Function_List = string.Empty;

            //Current Reporting VS New Reporting
            //source_Compare_Tables_List = "'" + dal.GetData("SELECT STRING_AGG(cast([SourceName] as varchar(max)), ''',''') FROM Currenet_VS_New_TableMaster").Rows[0][0].ToString() + "'";
            //destination_Compare_Tables_List = "'" + dal.GetData("SELECT STRING_AGG(isnull(cast([DestinationName] as varchar(MAX)),cast([SourceName] as varchar(max))) , ''',''') FROM Currenet_VS_New_TableMaster").Rows[0][0].ToString() + "'";
            //dt_Compare_View_List = "'" + dal.GetData("SELECT STRING_AGG(cast([Name] as varchar(max)), ''',''') FROM Currenet_VS_New_ViewMaster").Rows[0][0].ToString() + "'";
            //dt_Compare_SP_List = "'" + dal.GetData("SELECT STRING_AGG(cast([Name] as varchar(max)), ''',''') FROM Currenet_VS_New_SPMaster").Rows[0][0].ToString() + "'";
            //dt_Compare_Function_List = "'" + dal.GetData("SELECT STRING_AGG(cast([Name] as varchar(max)), ''',''') FROM Currenet_VS_New_FunctionMaster").Rows[0][0].ToString() + "'";

            //Abshire PROD VS Current Reporting
            //source_Compare_Tables_List = "'" + dal.GetData("SELECT STRING_AGG(cast([SourceName] as varchar(max)) , ''',''') FROM AsHirePROD_VS_Current_TableMaster").Rows[0][0].ToString() + "'";
            //destination_Compare_Tables_List = "'" + dal.GetData("SELECT STRING_AGG(isnull([DestinationName],cast([SourceName] as varchar(max))) , ''',''') FROM AsHirePROD_VS_Current_TableMaster").Rows[0][0].ToString() + "'";
            ////dt_Compare_View_List = "'" + dal.GetData("SELECT STRING_AGG(cast([Name] as varchar(max)), ''',''') FROM ViewMaster").Rows[0][0].ToString() + "'";
            ////dt_Compare_SP_List = "'" + dal.GetData("SELECT STRING_AGG(cast([Name] as varchar(max)), ''',''') FROM SPMaster").Rows[0][0].ToString() + "'";
            ////dt_Compare_Function_List = "'" + dal.GetData("SELECT STRING_AGG(cast([Name] as varchar(max)), ''',''') FROM FunctionMaster").Rows[0][0].ToString() + "'";

            if (IsTableCompare)
            {
                Console.WriteLine("Table comparison is running...");
                CompareTables compareTables = new CompareTables(List_Source_Tables, List_Destination_Tables, dtExcelData_CompareTables);
                //compareTables.Compare();
                Console.WriteLine("Table comparison is Done");
                Console.WriteLine("");
            }

            if (IsViewCompare)
            {
                Console.WriteLine("View comparison is running...");
                CompareViews compareViews = new CompareViews(List_View, dtExcelData_CompareViews);
                //compareViews.Compare();
                Console.WriteLine("View comparison is Done");
                Console.WriteLine("");
            }

            if (IsColumnCompare)
            {
                Console.WriteLine("Column comparison is running...");
                CompareColumns compareColumns = new CompareColumns(List_Source_Tables, List_Destination_Tables, dtExcelData_CompareColumns);
                //compareColumns.Compare();
                Console.WriteLine("Column comparison is Done");
                Console.WriteLine("");
            }

            if (IsIndexCompare)
            {
                Console.WriteLine("Index comparison is running...");
                CompareTableIndexes compareTableIndexes = new CompareTableIndexes(List_Source_Tables, List_Destination_Tables, dtExcelData_CompareTableIndexes);
                //compareTableIndexes.Compare();
                Console.WriteLine("Index comparison is Done");
                Console.WriteLine("");
            }

            if (IsSPCompare)
            {
                Console.WriteLine("Store Procedure comparison is running...");
                CompareSPs compareSPs = new CompareSPs(List_SP, dtExcelData_CompareSps);
                //compareSPs.Compare();
                Console.WriteLine("Store Procedure comparison is Done");
                Console.WriteLine("");
            }

            if (IsFunctionCompare)
            {
                Console.WriteLine("Function comparison is running...");
                CompareFunction compareFunction = new CompareFunction(List_Function, dtExcelData_CompareFunctions);
                //compareFunction.Compare();
                Console.WriteLine("Function comparison is Done");
                Console.WriteLine("");
            }

            Console.WriteLine("Excel report is Preparing...");
            ExcelWriter.CreateExcelFile(dtExcelData_CompareTables, dtExcelData_CompareViews, dtExcelData_CompareColumns, dtExcelData_CompareTableIndexes, dtExcelData_CompareSps, dtExcelData_CompareFunctions);
        }

    }
}
